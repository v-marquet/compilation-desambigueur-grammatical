#!/bin/bash

# this script compile and launch the lex file, not the bison file

if [[  ! "$1" ]]; then
	lex "main.l"
	cc lex.yy.c -o main -ll 
	./main
else
	lex "main.l"
	cc lex.yy.c -o main -ll  
	./main "$1"
fi



exit 0
