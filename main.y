%{
#include <stdio.h>
char** grammaire = NULL;
%}

%token START END
%token loi

%%

program : START loiList END { printf("loi"); }
;
loiList : loiList loi
        | loi
;


%%

int main(int argc, char** argv) {
	printf("Start syntaxic analysis...\n");

	return 0;
}
int yywrap() {  // yywrap() is called when there is an EOF (End Of File)
	return 1;   // return 1 is default return value. If we want more input before lex exit, then use return 0
}