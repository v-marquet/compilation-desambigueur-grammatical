//COMPILER : gcc -g -Wall desambigueur.c -o desambigueur
//EXECUTER : ./desambigueur grammaire.txt


#include<stdio.h>
#include<stdlib.h> 
#include<string.h>

//print le tableau 2D de la grammaire
void printArray( int lin, int col, char array[][col] ){
	int i, j=0;
	printf("\n");
	for( i=0; i < lin; i++ ){
		if(array[i][0]!=' '){
	
		if(array[i][0]==-1){
		
				printf("%c'", array[i][1]);
		}
		else{
		
				printf("%c", array[i][1]);
		}
	
	
			for( j=2; j < col; j++ ){
			
				if(j==2) printf("--> %c ", array[i][j]);
				if(j!=1 && j!=2) printf("%c ", array[i][j]);
			}
			printf("\n");
		}
	}
	printf("\n\n");
}

//initialise le tableau de la grammaire avec des espaces : ' '
void initArray(int lin, int col, char array[][col]){

	int i,j;
	for(i=0;i<lin;i++){
		for(j=0;j<col;j++){
			array[i][j]=' ';
		}
	}
}

//retourne 1 si on doit elimine la récursion gauche directe, 0 sinon
int doesWordNeedsGettingRidOfDirectLeftRec( int wordNumber, int lin, int col, char array[][col]){

	int i;

	for( i=0; i<lin; i++){
	
		if( array[i][0] == wordNumber+48 && array[i][1] == array[i][2] ){
			return 1;
		}
	}
	
	return 0;
}

//Elimine la récursivité gauche directe
int gettingRidOfDirectLeftRec ( int wordNumber, int length, int lin, int col, char array[][col]){

	int i;
	int k=3;
	int compteur1=0,compteur2=0;
	int tmp;
	int boolean=1;
	int boolean2=0;
	int valueToReturn=0;
	
	for( i=0; i<lin; i++){
	
		if( array[i][0] == wordNumber+48){
		
			if(boolean2==0){
				array[length][0]=-1;
				array[length][1]=array[i][1];
				length++;
				valueToReturn++;
				boolean2=1;
			}
		
			if( array[i][1] == array[i][2] ){
				compteur1++;
				k--;
				while( array[i][k] != ' '){
					array[i][k-1]=array[i][k];
					k++;
				}
				array[i][0]   = -1;
				array[i][k-1] = array[i][1];
				array[i][k]   = 39;
				k=3;
			}
			else{
			
				if( array[i][2] == ' ' ){
					boolean=0;
					array[i][0]=wordNumber+48;
					array[i][2]=array[i][1];
					array[i][3]=39;
				}
			
				else{
					compteur2++;
					while( array[i][k] != ' '){
						k++;
					}
					array[i][k]   = array[i][1];
					array[i][k+1] = 39;
					k=3;
				}
			}
			tmp=i;
		}
	}
	
	if(compteur1>0 && compteur2==0 && boolean==1){
		array[length][0]=wordNumber+48;
		array[length][1]=array[tmp][1];
		array[length][2]=array[tmp][1];
		array[length][3]=39;
		return valueToReturn+1;
	}
	if(compteur2>0 && compteur1==0 && boolean==1){
		array[length][0]=-1;
		array[length][1]=array[tmp][1];
		array[length][2]=array[tmp][1];
		array[length][3]=39;
		return valueToReturn+1;
	}
	
	return valueToReturn;
}

//Met à jour les autres règles
int updateWordj( int wordThatisUpdated ,int wordNumber, int length, int lin, int col, char array[][col] ){

	int i, j, k=0, l=2, z=3;
	int compteur=0;
	int weHaveToRomove=-1;
	
	for( i=0; i<length; i++ ){
	
		if( array[i][0] == wordNumber+48 && array[i][3]!=39){
			
			for( j=0; j<length; j++ ){
				
				if(	array[j][0] == wordThatisUpdated+48 && array[i][2] == array[j][1] ){
					l=2; z=3;
					weHaveToRomove = i;
					compteur++;
					array[length+k][0]=array[i][0];
					array[length+k][1]=array[i][1];
					
					while( array[j][l] != ' ' ){
						array[length+k][l] = array[j][l];
						l++;
					}
					while( array[i][z] != ' ' ){
						
						array[length+k][l] = array[i][z];
						l++;
						z++;
					}
					k++;
				}	
			}			
		}
	}

	if(weHaveToRomove!=-1){
		z=1;
		array[weHaveToRomove][0]=' ';
		while(array[weHaveToRomove][z]!=' '){

			array[weHaveToRomove][z]=' ';
			z++;
		}
	}
	return compteur;
}

int main(int argc, char** argv){
	
	int i,j;
	
	int a;

	if (argc <= 1) {
		printf("Précisez le fichier en paramètre \n");
		exit(0);
	}
	

	FILE* in = fopen(argv[1], "r");
	if (in == NULL) {
		printf("File not found \n");
		exit(0);
	}

	// on crée un tableau pour stocker la grammaire
	const int x = 27;
	const int y = 16;
	char grammar[x][y];
	int length = 0;
	int numberOfRules = 0;
	

	// on initialise le tableau
	initArray(x,y,grammar);

	// on scanne le fichier en entrée
	char* buffer = malloc(sizeof(char)*100);  // buffer overflow possible
	while (fscanf(in, "%s", buffer)!=EOF) {
		// on cherche si le membre gauche existe déjà dans le tableau
		char num_column_1; int found = 0;
		char max = '/';  // caractère juste en dessous du '0' en ASCII
		for (a=0; a<length; a++) {
			if (grammar[a][0] > max)
				max = grammar[a][0];
			if (buffer[0] == grammar[a][1]) {
				num_column_1 = grammar[a][0];
				found = 1;
				break;
			}
		}

		// s'il existe déjà
		if (found == 1) {
			grammar[length][0] = num_column_1;
			for (a=0; a<strlen(buffer); a++) {
				grammar[length][a+1] = buffer[a];
			}
			length++;
		}

		// s'il n'existe pas déjà
		if (found == 0) {
			max++;
			numberOfRules = max + 1;
			grammar[length][0] = max;
			for (a=0; a<strlen(buffer); a++) {
				grammar[length][a+1] = buffer[a];
			}
			length++;
		}
	}
	free(buffer);
	
	
	//On affiche la grammaire extraite du fichier texte
	printf("\nOld Grammar :\n"); 
	printArray( length, 16, grammar );
	
	numberOfRules-=48;
	
	//Pour chacunes des regles
	for(i=0; i<numberOfRules; i++){

		//Si on a besoin d'eliminer la récurrence gauche directe d'une règles alors on l'elimine
		if(doesWordNeedsGettingRidOfDirectLeftRec( i, length, 16, grammar )==1) length+=gettingRidOfDirectLeftRec( i, length, length, 16, grammar );

		//Pour toutes les autres règles (sauf celles déjà traitées)
		for(j=i+1; j<numberOfRules; j++){

			//On met à jour ces règles
			length+=updateWordj( i, j, length, 27, 16, grammar );
			
		}	
	}


	//On affiche la nouvelle grammaire sans récurrence gauche directe et indirecte
	printf("Grammar without Left Recursion (direct+indirect)\n");
	printArray( length, 16, grammar );
	
	

	return 0;
}
