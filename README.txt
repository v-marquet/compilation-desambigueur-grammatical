I) Etat d'avancement des travaux

Etant donné notre manque d'expérience avec Flex, et le fait que nous n'ayons jamais utilisé Bison, nous avons commencé par regarder beaucoup de documentation et d'exemples d'utilisation sur internet. Cependant, après plusieurs heures passées à chercher sur internet sans trouver suffisamment d'explications pour utiliser correctement Flex et Bison pour faire ce TP, nous avons décidé de faire l'élimination de la récursivité gauche et la factorisation entièrement en C, afin d'avoir le temps de réfléchir aux algorithmes du Dragon's Book et d'avoir un programme fonctionnel, plutôt que de risquer de n'avoir aucun résultat via Flex et Bison. 

Etant deux assez bon programmeurs dans le groupe, nous avons décidé de faire chacun notre version du programme, et de prendre ensuite la meilleure. Cela nous donnait aussi l'avantage de pouvoir comparer nos résultats afin de tester nos programmes. Effectivement, l'algorithme de l'élimination de la récursivité gauche du Dragon's Book était ambigu pour certaines personnes, donc il était important de comparer nos résultats. 

L'un de nous a choisi de stocker la grammaire dans un char***, l'autre plus simplement dans un tableau statique à deux dimensions. Malheureusement, la version avec le char*** fonctionnait pour éliminer la récursivité gauche dans certains cas, mais pas dans d'autres cas, et le débugage était extrèmement compliqué, car la moindre erreur de pointeur était fatale. Nous avons donc décidé de garder la version avec un tableau statique à deux dimensions, afin d'avoir le temps de nous concentrer sur la factorisation, plutôt que de perdre du temps à débugguer. 

Nous avons donc implémenté l'algorithme d'élimination de la récursivité gauche directe et indirecte, donné dans le Dragon's Book. La récursivité gauche a été traité entièrement : nous avons aussi géré le cas où la grammaire de base a des expressions vides (cf nos exemples de grammaires). Cependant, la factorisation a été faite partiellement car certains exemples tordus ne passaient pas... C'est pourquoi nous avons décidé de mettre notre code de factorisation dans un autre fichier pour que vous puissiez y jeter un oeil.

Nous avons également mis le fichier lex (main.l) que nous avions commencé à faire, avant de décider de tout faire en C. Ce fichier nous permettait de détecter les caracères ASCII utilisés pour les productions, et de détecter les retours à la ligne qui séparent les productions dans le fichier en entrée.


II) Références aux sources

Pour l'élimination de la récursivité gauche, nous avons surtout utilisé l'algorithme du Dragon's Book, étant donné que c'est un livre de référence. De plus, pour les exemples, nous avons pris des grammaires proposées dans ce polycopié en ligne : http://cours.do.am/_ld/0/33_Chap4-Compilati.pdf.


III) Fonctionnalités supplémentaires

Bien sûr, nous regrettons de ne pas avoir eu le temps d'apprendre à se servir correctement de Flex et Bison, donc si nous avions eu du temps en plus, nous aurions intégré notre code en C dans les programmes Flex et Bison. L'amélioration auquelle nous avons pensé est de déterminer si la grammaire est LL(1) ou non.
