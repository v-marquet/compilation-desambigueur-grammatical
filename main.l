%{
#include <stdio.h>
#include "main.tab.h"
%}

loi [A-Za-z0-9"'`<>()\[\]{}|!?&#$%+-=_^*~@.,;:/\\]+
endline [\n]

%%
{loi}		printf("LOI<%s>\n\n", yytext);
%%

int main(int argc, char** argv) {
	printf("Start lexical analysis...\n");
	if(argc>1)  // ça ne marche pas, donc utiliser le chevron
		yyin=fopen(argv[1],"r");
	yylex();
	return 0;
}
int yywrap() {  // yywrap() is called when there is an EOF (End Of File)
	return 1;   // return 1 is default return value. If we want more input before lex exit, then use return 0
}