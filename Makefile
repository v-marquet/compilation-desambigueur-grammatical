LEX = flex
YACC = bison -d
CC = gcc

main: main.tab.c lex.yy.c
	$(CC) -o main main.tab.c lex.yy.c
	
main.tab.c: main.y
	$(YACC) main.y

lex.yy.c: main.l
	$(LEX) main.l
