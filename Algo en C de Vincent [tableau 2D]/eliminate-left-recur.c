#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// #define DEBUG

void drawTab(char** tab, int* tab_len);
void removeRule(char* tab, int start_remove);
char* getRule(char* tab, int start_alpha);
void appendWithPipe(char* base, char* append);
void appendWithoutPipe(char* base, char* append);
void eliminateLeftRecur(char* string, char** tab_new, int* tab_new_len);


int main() {
	int i,j,k,l;

	int* tab_len = malloc(sizeof(int)*2);
	tab_len[0] = 2;  // warning: buffer overflow
	tab_len[1] = 50;
	int* tab_new_len = malloc(sizeof(int)*3);
	tab_new_len[0] = 50;
	tab_new_len[1] = 50; // ceci n'est alloué qu"au fur et à mesure
	tab_new_len[2] = 0;  // nombre d'allocations au niveau inférieur

	// allocation statisque du tableau
	int tab_old_len_1 = 2;
	char tab_old[2][10]={
					{'S','|','A','a','|','b','\0','\0','\0','\0'},
					{'A','|','A','c','|','S','d','|','c','\0'},
				   };

	// allocation dynamique du tableau
	char** tab = malloc(sizeof(char*)*tab_len[0]);
	for (i=0; i<tab_len[0]; i++) {
		tab[i] = malloc(sizeof(char)*tab_len[1]);
		for (j=0; j<tab_len[1]; j++)
			tab[i][j] = '\0';
	}

	// allocation dynamique du tableau final
	char** tab_new = malloc(sizeof(char*)*tab_new_len[0]);
	// for (i=0; i<tab_new_len[0]; i++) {
	// 	tab_new[i] = malloc(sizeof(char)*tab_new_len[1]);
	// 	for (j=0; j<tab_new_len[1]; j++)
	// 		tab_new[i][j] = '\0';
	// }
	for (i=0; i<tab_new_len[0]; i++) {
		tab_new[i] = NULL;
	}

	// PROVISOIRE, il faudrait plutôt lire le fichier
	for (i=0; i<tab_old_len_1; i++) {
		j=0;
		while (tab_old[i][j] != '\0') {
			tab[i][j] = tab_old[i][j];
			j++;
		}
	}
	// FIN PROVISOIRE

	drawTab(tab, tab_len);

	for (i=0; i<tab_len[0]; i++) {
		char A_i = tab[i][0];
		for (j=0; j<i; j++) {
			char A_j = tab[j][0];

			// on cherche les A_i -> A_j alpha
			k = 1;
			while (tab[i][k+1] != '\0') {
				if (tab[i][k] == '|' && tab[i][k+1] == A_j) {
					printf("A%d -> A%d alpha \n", i, j);

					// we get the alpha
					char* alpha = getRule(tab[i], k+2);
					printf("alpha = %s \n", alpha);

					// we remove this rule
					removeRule(tab[i], k);

					// we append all new rules with delta alpha
					l = 1;
					while (tab[j][l] != '\0') {
						if (tab[j][l] == '|') {

							// we get the delta
							char* delta = getRule(tab[j], l+1);
							printf("delta = %s \n", delta);

							// we append delta alpha to A_i
							appendWithPipe(tab[i], delta);
							appendWithoutPipe(tab[i], alpha);

							free(delta);
							drawTab(tab, tab_len);
						}
						l++;
					}


					free(alpha);  // free le malloc de getAlpha()
					drawTab(tab, tab_len);
				}

				k++;
			}
		}
		eliminateLeftRecur(tab[i], tab_new, tab_new_len);
	}

	printf("--- FIN ---   \n");
	drawTab(tab, tab_len);
	drawTab(tab_new, tab_new_len);

	free(tab_len);
	free(tab_new_len);
	return 0;

}

void drawTab(char** tab, int* tab_len) {
	int i=0;
	for (i=0; i<tab_len[0]; i++) {
		if (tab[i] == NULL)
			break;
		int j=0;
		while (tab[i][j] != '\0') {
			printf("%c", tab[i][j]);
			j++;
		}
		printf("\n");
	}
	printf("---------\n");
	return;
}

void removeRule(char* tab, int start_remove) {
	printf("Removing rule... \n");
	int rule_len = 0;
	int i = start_remove;  // start_remove is at the index of the '|' before rule 
	i++;  // i is now at the index of the first letter of the rule
	while (tab[i] != '|' && tab[i] != '\0') {
		rule_len++;  // the letter is not the end of the rule, so it's still the rule
		i++;
	}
	if (tab[i] == '\0') {  // il suffit de remplacer les cases par '\0'
		int j;
		for (j=start_remove; j<rule_len+1; j++)
			tab[j] = '\0';
	}
	if (tab[i] == '|') {
		// on décale vers la gauche
		while (tab[i] != '\0') {
			tab[i-(rule_len+1)] = tab[i];  
			i++;
		}
		// on finit de remplir avec '\0'
		int j;
		for (j=i; j<i+(rule_len+1) ;j++) {
			tab[i-(rule_len+1)] = '\0';
		}
	}
	return;
}

// WARNING: don't forget to free the char* returned by getRule()
char* getRule(char* tab, int start_rule) { // to get next rule, pointer start_rule must be on the first char
	#ifdef DEBUG
		printf("entering getRule... \n");
	#endif

	int start = start_rule;
	int rule_len = 0;
	while (tab[start] != '\0' && tab[start] != '|') {
		rule_len++;
		start++;
	}
	char* rule = malloc(sizeof(char)*(rule_len+1)); // +1 pour le '\0'
	int i;
	for (i=start_rule; i<start_rule+rule_len; i++)
		rule[i-start_rule] = tab[i];
	rule[rule_len] = '\0';
	// printf("Rule = %s \n", rule);

	#ifdef DEBUG
		printf("leaving getRule \n");
	#endif
	return rule;
}


void appendWithPipe(char* base, char* append) {  // WARNING: it puts a pipe before "append"
	#ifdef DEBUG
		printf("entering appendWithPipe... \n");
	#endif

	if (base == NULL) {
		printf("WARNING: base = NULL \n");
	}

	int i=0; int j;
	while (base[i] != '\0') {
		i++;
	}
	base[i] = '|';
	for (j=i+1; j<i+1+strlen(append); j++) {
		base[j] = append[j-(i+1)];
	}

	#ifdef DEBUG
		printf("leaving appendWithPipe \n");
	#endif
	return;
}

void appendWithoutPipe(char* base, char* append) {
	#ifdef DEBUG
		printf("entering appendWithoutPipe... \n");
	#endif

	int i=0; int j;
	while (base[i] != '\0') {
		i++;
	}
	for (j=i; j<i+strlen(append); j++) {
		base[j] = append[j-i];
	}

	#ifdef DEBUG
		printf("leaving appendWithoutPipe \n");
	#endif
	return;
}

void eliminateLeftRecur(char* string, char** tab_new, int* tab_new_len) {
	#ifdef DEBUG
		printf("entering eliminateLeftRecur... \n");
	#endif

	char* prime = malloc(sizeof(char)*2);
	prime[0] = 'X';
	prime[1] = '\0';

	char left_member = string[0];

	int i=1; int is_there_left_recur = 0;
	while (string[i] != '\0') {
		if (string[i] == '|' && string[i+1] == left_member) {
			is_there_left_recur = 1;
			break;
		}
		i++;
	}

	if (is_there_left_recur == 1) {

		char* A = malloc(sizeof(char)*tab_new_len[1]); A[0] = left_member;
		char* A_prime = malloc(sizeof(char)*tab_new_len[1]); A_prime[0] = prime[0]; // PROVISOIRE

		i=1;
		while (string[i] != '\0') {
			// we search for new rules
			if (string[i] == '|') {
				if (string[i+1] == left_member) {  // A -> A alpha
					char* rule = getRule(string, i+2);
					appendWithPipe(A_prime, rule);
					appendWithoutPipe(A_prime, prime);
					free(rule);
				}
				else {  // A -> beta
					char* rule = getRule(string, i+1);
					appendWithPipe(A, rule);
					appendWithoutPipe(A, prime);
					free(rule);
				}
			}
			i++;
		}

		// on met A et A_prime dans la liste finale
		int n = tab_new_len[2];
		tab_new[n] = A;
		tab_new[n+1] = A_prime;
		tab_new_len[2] += 2;
	}

	if (is_there_left_recur == 0) {
		// on copie tou simplement string 
		char* A = malloc(sizeof(char)*tab_new_len[1]);

		i=0;
		while (string[i] != '\0') {
			A[i] = string[i];
			i++;
		}
		A[i] = '\0';

		// on met A dans la liste finale
		int n = tab_new_len[2];
		tab_new[n] = A;
		tab_new_len[2] += 1;
	}

	


	#ifdef DEBUG
		printf("leaving eliminateLeftRecur \n");
	#endif
	return;
}